#! /usr/bin/env python
#-*-coding: utf-8-*-

import cv2
import numpy as np

angle = 0
tx = 0
ty = 0
scale = 1.0 



#############################################################


drawing = False #true if mouse is pressed
#mode = True # if True, draw rectangle. Press ’m’ to toggle to curve

ix,iy = -1,-1
fx,fy = -1,-1

xy_pers = []
xy = []
puntos_imagen2 = []
puntos_perspect2 = []

def draw_rectangule ( event ,x , y, flags, param ) :
	global ix , iy, fx,fy , drawing
	if event == cv2.EVENT_LBUTTONDOWN:
		drawing = True
		ix , iy = x , y

	elif event == cv2.EVENT_LBUTTONUP:
		fx , fy = x , y
		drawing = False
		#if mode is True :
		cv2.rectangle(img, (ix,iy),(fx,fy),(100,100,100),1)
		for row in range(len(img)):
			for col in range(len(img[row])):
				if (ix < col < fx)and (iy < row < fy) :
					img_nueva[row][col] = img[row][col]
				else:
					img_nueva[row][col] = 0
		cv2.destroyAllWindows()
		cv2.imshow ('image',img_nueva)
		#cv2.waitKey(0)
		#else :
		#	cv2.circle(img,(x,y),5,(0,0,0),2)

	#elif event == cv2.EVENT_MOUSEMOVE:
	#	if drawing is True:
	#		cv2.rectangle(img, (ix ,iy),(fx ,fy),(0,0,0),2)




def draw_circle ( event ,x , y, flags, param ) :
	global xy, drawing
	#if event == cv2.EVENT_LBUTTONDOWN:
	#	drawing = True
	#	ix , iy = x , y

	if event == cv2.EVENT_LBUTTONUP:
		if len(xy) < 3:
			xy.append((x,y))
			print(xy)
			#drawing = False
			#if mode is True :
			cv2.circle( img_perspectiva , (x,y) , 1 , (0,0,0) , 2)

		#for row in range(len(img)):
		#	for col in range(len(img[row])):
		#		if (ix < col < fx)and (iy < row < fy) :
		#			img_nueva[row][col] = img[row][col]
		#		else:
		#			img_nueva[row][col] = 0
		cv2.destroyAllWindows()
		cv2.imshow ('image',img_perspectiva)
		#cv2.waitKey(0)
		#else :
		#	cv2.circle(img,(x,y),5,(0,0,0),2)

	#elif event == cv2.EVENT_MOUSEMOVE:
	#	if drawing is True:
	#		cv2.rectangle(img, (ix ,iy),(fx ,fy),(0,0,0),2)

def draw_circle_4 ( event ,x , y, flags, param ) :
	global xy, drawing
	#if event == cv2.EVENT_LBUTTONDOWN:
	#	drawing = True
	#	ix , iy = x , y

	if event == cv2.EVENT_LBUTTONUP:
		if len(xy_pers) < 4:
			xy_pers.append((x,y))
			print(xy_pers)
			#drawing = False
			#if mode is True :
			cv2.circle( img_perspectiva , (x,y) , 1 , (0,0,0) , 2)

		#for row in range(len(img)):
		#	for col in range(len(img[row])):
		#		if (ix < col < fx)and (iy < row < fy) :
		#			img_nueva[row][col] = img[row][col]
		#		else:
		#			img_nueva[row][col] = 0
		cv2.destroyAllWindows()
		cv2.imshow ('image',img_perspectiva)
		#cv2.waitKey(0)
		#else :
		#	cv2.circle(img,(x,y),5,(0,0,0),2)

	#elif event == cv2.EVENT_MOUSEMOVE:
	#	if drawing is True:
	#		cv2.rectangle(img, (ix ,iy),(fx ,fy),(0,0,0),2)



def translate(img_nueva, tx, ty):
	M = np.float32([[ 1, 0, tx],
			[0,1,ty]])


	shifted = cv2.warpAffine(img_nueva, M, (w,h))
	return shifted 

def rotate(dst, angle, center , scale):

	if center is None:
		center = (w/2 ,h/2)
	

	S = cv2.getRotationMatrix2D (center ,angle , scale)
	rotated = cv2.warpAffine(dst, S, (w, h))

	return rotated


#img = np.zeros((512, 512, 3),np.uint8)

#img = cv2.imread('arco2.jpg')
#img_nueva = cv2.imread('arco2.jpg')
imagen2 = cv2.imread('pelota.png')
img_ = cv2.imread('perspectiva.jpg')
img_new = cv2.imread('perspectiva.jpg')

width = 800
height = 600

# Redimensionar la imagen
img_perspectiva = cv2.resize(img_, (width, height))
img_perspectiva_new = cv2.resize(img_new, (width, height))

cv2.namedWindow('image')

cv2.setMouseCallback('image',draw_rectangule)

cv2.imshow ('image',img_perspectiva)

h_img2, w_img2, _ = imagen2.shape
puntos_imagen2 = np.float32([[0, 0], [w_img2-1, 0], [0, h_img2-1]])


while(1) :
	
	k = cv2.waitKey(1) & 0xFF
	if k == 113 : #Salir, letra q
		break

	if k == 103 : #Guardar recorte, letra g
		
		cv2.imwrite('recorte.png',img_perspectiva_new)

		
		#cv2.destroyWindow('image')

		
	if k == 101 : #Rotacion y traslacion, letra e
		angle = 26
		tx = 100
		ty = 100
		scale = 1

		(h,w) = (img_perspectiva_new.shape[0], img_perspectiva_new.shape[1])
		center = (w/2 , h/2)

		dst = translate(img_perspectiva_new, tx, ty)

		(h,w) = (dst.shape[0], dst.shape[1])
		center = (w/2 , h/2)

		Tras_Rot = rotate(dst, angle, center, scale) 

		cv2.imwrite('traslado_rotacion.png',Tras_Rot)
		
		cv2.destroyAllWindows()
		cv2.imshow ('image',Tras_Rot)
		#cv2.waitKey(0)

	if k == 115 : # Escalado, letra s
		angle = 0
		scale = 2

		(h,w) = (Tras_Rot.shape[0], Tras_Rot.shape[1])
		center = (w/2 , h/2)

		Escalado = rotate(Tras_Rot, angle, center , scale) 

		cv2.imwrite('escalado.png',Escalado)
		cv2.destroyAllWindows()
		cv2.imshow ('image',Escalado)
		

	if k == 114: # Recuperar foto inicial, letra r
		cv2.destroyAllWindows()
		
		img_ = cv2.imread('perspectiva.jpg')
		img_perspectiva = cv2.resize(img_, (width, height))
		cv2.namedWindow('image')
		cv2.setMouseCallback('image',draw_circle)
		cv2.imshow ('image',img_perspectiva)
	
	if k == 97: # Letra a para seleccionar 3 puntos no colineales
				
		cv2.setMouseCallback('image',draw_circle)
		cv2.imshow ('image',img_perspectiva)
		if len(xy) == 3:	
			# Convertir la lista de puntos a una matriz de NumPy (2x3)
			puntos_imagen1 = np.float32(xy)
			puntos_imagen2_float = np.float32(puntos_imagen2)

			#print(puntos_imagen1)
			#print(puntos_imagen2_float)

			# Calcular la matriz de transformación afín
			M_affin = cv2.getAffineTransform(puntos_imagen2_float, puntos_imagen1)
			#print(M_affin)

			# Aplicar la transformación afín a la segunda imagen
			transformada = cv2.warpAffine(imagen2, M_affin, (img_perspectiva.shape[1], img_perspectiva.shape[0]))

			# Crear una máscara para la segunda imagen
			mascara = np.zeros_like(img_perspectiva, dtype=np.uint8)
			cv2.fillConvexPoly(mascara, np.int32(puntos_imagen1), (255, 255, 255))

			# Invertir la máscara para borrar el área donde se incrustará la segunda imagen
			mascara_invertida = cv2.bitwise_not(mascara)

			# Borrar el área en la primera imagen
			imagen1_borrada = cv2.bitwise_and(img_perspectiva, mascara_invertida)

			# Combinar las imágenes
			resultado = cv2.add(imagen1_borrada, transformada)

			# Mostrar la imagen resultante
			cv2.destroyAllWindows()
			#cv2.imshow("Resultado1", transformada)
			#cv2.imshow("Resultado2", mascara_invertida)
			#cv2.imshow("Resultado3", imagen1_borrada)
			cv2.imshow("Resultado", resultado)
	if k == 104: # Letra h para seleccionar 4 puntos no colineales
		cv2.setMouseCallback('image',draw_circle_4)
		cv2.imshow ('image',img_perspectiva)
		if len(xy_pers) == 4:
	
			#h_pers, w_pers, _ = img_perspectiva_new.shape
			h_pers, w_pers = 300,400
			puntos_perspect2 = np.float32([[0, 0], [w_pers-1, 0],[w_pers-1, h_pers-1], [0, h_pers-1]])
			
			puntos_persp = np.float32(xy_pers)
			puntos_perspect2_float = np.float32(puntos_perspect2)
		
			M_p = cv2.getPerspectiveTransform(puntos_persp, puntos_perspect2_float)

			rectificada = cv2.warpPerspective(img_perspectiva, M_p, (h_pers,w_pers))
		
			cv2.destroyAllWindows()
			cv2.imshow("Resultado", rectificada)
			
			
			
		
cv2.destroyAllWindows()


