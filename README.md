# Practico 8 - Rectificando imágenes

[Rectificando_imagene.py](https://gitlab.com/vision-por-computadora615916/practico-8-rectificando-imagenes/-/blob/main/Rectificando_imagene.py?ref_type=heads) este script en Python utiliza OpenCV para permitir al usuario dibujar un rectángulo sobre una imagen, recortar la sección seleccionada y aplicar operaciones de traslación, rotación, escalado, transformación afín e incrustación de una segunda imagen, además de aplicar una transformación de perspectiva.

## Funcionalidad
Configuración Inicial: Importa las bibliotecas necesarias y define variables globales para el ángulo de rotación, desplazamiento y escala.

- Definición de Callback: Define varias funciones para manejar los eventos del ratón:

        draw_rectangule: Dibuja un rectángulo en la imagen y recorta la sección seleccionada.

        draw_circle: Permite seleccionar 3 puntos para realizar una transformación afín.

        draw_circle_4: Permite seleccionar 4 puntos para realizar una transformación de perspectiva.

- Funciones de Transformación: Define funciones para trasladar (translate), rotar (rotate) y realizar transformaciones afines y de perspectiva sobre la imagen.

- Carga de Imagen: Carga las imágenes `perspectiva.jpg` y `pelota.png` para el procesamiento, y redimensiona la imagen principal.


Bucle Principal: En un bucle, muestra la imagen y escucha las teclas:

        q: Salir del programa.

        g: Guardar el recorte del rectángulo en recorte.png.

        e: Aplicar traslación y rotación, y guardar el resultado en traslado_rotacion.png.

        s: Aplicar escalado y guardar el resultado en escalado.png.

        r: Reiniciar la imagen para empezar de nuevo.

        a: Seleccionar 3 puntos para realizar una transformación afín e incrustar la segunda imagen.

        h: Seleccionar 4 puntos para realizar una transformación de perspectiva.